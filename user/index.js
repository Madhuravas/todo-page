let options = {
    method: "GET",
    headers: {
        "Content-Type": "application/json"
    }
}

let url = "http://localhost:9000/"

fetch(url, options)
    .then((res) => {
        return res.json()
    }).then((data) => {
        data.forEach((eachTodo, index) => {
            createAndAppendTodo(eachTodo, index)
        });
    })


const userInputEl = document.getElementById("userInput")
const addButtonEl = document.getElementById("addButton")

const tasksContainerEl = document.getElementById("tasksContainer")



addButtonEl.onclick = async () => {
    let url = "http://localhost:9000/"
    const response = await fetch(url)
    const data = await response.json()
    
    let id = data.length
    let userData = {
        id: id,
        userTask: userInputEl.value,
        isChecked: false
    }


    createAndAppendTodo(userData)
    addDataToServer(userData)

    if (userInputEl.value === "") {
        alert("Please add content")
    }

    userInputEl.value = ""
}



let checkBoxChecked = (taskCard, index) => {
    taskCard.classList.toggle("checked")

    let options = {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        }
    }
    
    let url = `http://localhost:9000/todos/update/${index}`
    
    fetch(url, options)
}


function deleteTodo(taskCard, index) {
    tasksContainerEl.removeChild(taskCard)
    
    let options = {
        method: "DELETE",
        headers: {
            "Content-Type": "application/json"
        }
    }
    
    let url = `http://localhost:9000/todos/${index}`
    
    fetch(url, options)
}


function createAndAppendTodo(todo, index) {
    let checkboxId = "checkbox" + todo.id;
    let todoId = "todo" + todo.id
    let taskTextId = "taskText" + todo.id

    let taskCard = document.createElement("li")
    taskCard.classList.add("task-card")
    taskCard.id = todoId

    let checkBoxEl = document.createElement("input")
    checkBoxEl.type = "checkbox"
    checkBoxEl.id = checkboxId
    checkBoxEl.checked = todo.isChecked
    checkBoxEl.classList.add("check-box")



    taskCard.appendChild(checkBoxEl)

    let userTaskEl = document.createElement("div")
    userTaskEl.classList.add("user-task")

    let taskTextEl = document.createElement("label")
    taskTextEl.setAttribute("for", checkboxId)
    taskTextEl.textContent = todo.userTask
    
    checkBoxEl.onclick = () => {
        checkBoxChecked(taskTextEl, index)
    }

    if (todo.isChecked) {
        taskTextEl.classList.add("checked")
    }

    taskTextEl.id = taskTextId

    taskTextEl.classList.add("task")


    userTaskEl.appendChild(taskTextEl)


    let deleteIconEl = document.createElement("i")
    deleteIconEl.classList.add("fa", "fa-trash")

    deleteIconEl.onclick = function () {
        deleteTodo(taskCard, index)
    }


    userTaskEl.appendChild(deleteIconEl)

    taskCard.appendChild(userTaskEl)
    tasksContainerEl.appendChild(taskCard)

}

function addDataToServer(userData) {

    let options = {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(userData)
    }

    let url = "http://localhost:9000/todos"

    fetch(url, options)
}


