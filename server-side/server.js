const express = require("express");
const app = express();
const fs = require("fs")
const cors = require("cors");
const { request } = require("http");
const { response } = require("express");

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))



//let todos = fs.readFile("data.json","utf-8")

app.get("/", (request, response) => {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            response.send(err)
        } else {
            response.send(JSON.parse(data))
        }
    })
})


app.post("/todos", (request, response) => {
    const todo = request.body
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            response.send(err.message)
        } else {
            let parseData = JSON.parse(data)
            parseData.push(todo)
            fs.writeFile("data.json", JSON.stringify(parseData), (err) => {
                if (err) {
                    response.send(err.message)
                }
            })
        }
    })
})



app.put("/todos/update/:Index/", async (request, response) => {
    const { Index } = request.params;
    let index = parseInt(Index)
    fs.readFile("data.json", "utf-8",(err,data) =>{
        if(err){
            response.send(err)
        }else{
            let parseData = JSON.parse(data)
            if (parseData[index].isChecked){
                parseData[index].isChecked = false
            }else{
                parseData[index].isChecked = true
            }
             
            
            fs.writeFile("data.json",JSON.stringify(parseData),(err)=>{
                if(err){
                    response.send(err.message)
                }
            })
        }
    })

    response.send("Todo Updated Successfully");
  });





app.delete("/todos/:Index/", async (request, response) => {
    const { Index } = request.params;
    let index = parseInt(Index)
    console.log(index)
    fs.readFile("data.json","utf-8",(err,data) =>{
        if(err){
            response.send(err)
        }else{
            let parseData = JSON.parse(data)
            parseData.splice(index,1)
            
            fs.writeFile("data.json",JSON.stringify(parseData),(err) =>{
                if(err){
                    response.send(err.message)
                }
            })
        }
    })
    response.send("Todo Deleted")
});



app.listen(9000, () => {
    console.log("Server runs at 9000")
})